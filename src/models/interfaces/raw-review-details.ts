import RawReview from './raw-review';

export default interface RawReviewDetails extends RawReview {
  body: string;
}
