import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Rating from './';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Rating rating={0.5} />, div);
});
