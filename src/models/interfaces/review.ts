export default interface Review {
  rating: number;
  publish_date: Date;
  id: string;
  author: string;
}