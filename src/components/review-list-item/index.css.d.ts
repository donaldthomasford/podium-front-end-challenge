export const item: string;
export const active: string;
export const left: string;
export const author: string;
export const dateWrapper: string;
export const date: string;
export const rating: string;
