import React, { SFC } from 'react';
import { Review } from '../../models';
import classnames from 'classnames';
import Rating from '../rating';

import styles from './index.css';

interface Props {
  review: Review;
  active?: boolean;
  onClick(): void;
}

const ReviewListItem: SFC<Props> = (props) => {
  const { review, onClick, active } = props;
  const { rating, publish_date, author } = review;

  const itemClasses = classnames(styles.item, {
    [styles.active]: active,
  });

  return (
    <div className={itemClasses} onClick={onClick} >
      <div className={styles.left}>
        <div className={styles.author}>
          {author}
        </div>
        <div className={styles.dateWrapper}>
          <div className={styles.date}>
            {publish_date.toLocaleDateString('en-US')}
          </div>
        </div>
      </div>
      <div className={styles.rating}>
        <Rating rating={rating}/>
      </div>
    </div>
  );
};

export default ReviewListItem;
