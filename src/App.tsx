import React, { Component, ChangeEvent } from 'react';
import { Review, ReviewDetails, SortDirection } from './models';
import { ReviewListItem, ReviewPanel } from './components';
import classnames from 'classnames';

import { fetchReviewList, fetchReview } from './services/reviews';

import styles from './index.css';

interface State {
  reviews: Review[];
  review: null | ReviewDetails;
  searchText: string;
  sortDirection: SortDirection;
}

class App extends Component<{}, State> {
  state = {
    reviews: [],
    review: null,
    searchText: '',
    sortDirection: SortDirection.none,
  };

  componentDidMount() {
    fetchReviewList().then(reviews => this.setState({ reviews }));
  }

  handleItemClick = (selectedId: string) => {
    fetchReview(selectedId).then((review: ReviewDetails) => {
      this.setState({ review: review });
    });
  }

  handleSortClick = (sort: SortDirection) => {
    this.setState({ sortDirection: sort === this.state.sortDirection ? SortDirection.none : sort });
  }

  getReviews(reviews: Review[], activeReview: ReviewDetails | null, sortDirection: SortDirection): JSX.Element[] {
    return reviews
      .filter(review => review.author.toLowerCase().includes(this.state.searchText.toLowerCase()))
      .sort((a, b) => {
        if (sortDirection === SortDirection.asc) { return a.rating < b.rating ? -1 : 1; }
        if (sortDirection === SortDirection.desc) { return a.rating > b.rating ? -1 : 1; }

        return 0;
      })
      .map(review => (
      <div className={styles.listItem} key={review.id}>
        <ReviewListItem
          review={review}
          active={activeReview !== null && activeReview.id === review.id}
          onClick={this.handleItemClick.bind(this, review.id)}
        />
      </div>
    ));
  }

  getReview(review: ReviewDetails | null): JSX.Element {
    return (review === null)
      ? <div className={styles.info}>Please Select An Item</div>
      : <ReviewPanel review={review}/>;
  }

  handleSearch = (event: ChangeEvent<HTMLInputElement>) => {
    this.setState({ searchText: event.target.value });
  }

  render() {
    const { reviews, review, sortDirection } = this.state;
    const ascClasses = classnames(styles.button, {
      [styles.activeButton]: sortDirection === SortDirection.asc,
    });
    const descClasses = classnames(styles.button, {
      [styles.activeButton]: sortDirection === SortDirection.desc,
    });

    return (
      <div className={styles.app}>
        <div className={styles.appHeader}>
          <div className={styles.appTitle}>
            Shakespeare Reviews App
          </div>
        </div>
        <div className={styles.appBody}>
          <div className={styles.listWrapper}>
            <div className={styles.search}>
              <input
                placeholder="Search.."
                className={styles.input}
                onChange={this.handleSearch}
              />
              <div className={styles.sort}>
                Rating
                <button className={ascClasses} onClick={this.handleSortClick.bind(this, SortDirection.asc)}>
                  Asc
                </button>
                <button className={descClasses} onClick={this.handleSortClick.bind(this, SortDirection.desc)}>
                  Desc
                </button>
              </div>
            </div>
            <div className={styles.list}>
              {this.getReviews(reviews, review, sortDirection)}
            </div>
          </div>
          <div className={styles.panel}>
            {this.getReview(review)}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
