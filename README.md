## Podium Front End Test

This is a repo that completes the front-end challenge presented by Podium [here](https://docs.google.com/document/d/1sPJG5UuTgDCjlTSZCXu_dPHBIDgEdEsEMDsZ7r76mWs/edit).

This app was created using [this fork](https://github.com/christikaes/create-react-app-typescript-css) of create react app.

In order to get it up and running, you just need to do the following

- clone the repo
- run `yarn` to install dependencies
- run `yarn start` and then the browser should open up the application (if it doesn't, navigate to http://localhost:3000/ to see it).

To run tests, simply run `yarn test`

Hope you enjoy reading all them Shakespeare Reviews now that you have some UI to go with your API!
