import React, { SFC } from 'react';
import classnames from 'classnames';

import styles from './index.css';

interface Props {
  rating: number;
}

const ReviewListItem: SFC<Props> = (props) => {
  const { rating } = props;

  const ratingClasses = classnames(styles.rating, {
    [styles.low]: rating < 1,
    [styles.mid]: rating < 3 && rating >= 1,
    [styles.high]: rating >= 3,
  });

  return (
    <div className={ratingClasses} >
      {rating}
    </div>
  );
};

export default ReviewListItem;
