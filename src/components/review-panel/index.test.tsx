import * as React from 'react';
import * as ReactDOM from 'react-dom';
import ReviewPanel from './';

import { ReviewDetails } from '../../models';

const review: ReviewDetails = {
  id: 'test',
  body: 'this is my review',
  author: 'Test Author',
  publish_date: new Date(),
  rating: 1.5,
};

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ReviewPanel review={review} />, div);
});
