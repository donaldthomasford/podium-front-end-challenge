import * as React from 'react';
import * as ReactDOM from 'react-dom';
import ReviewListItem from './';

import { Review } from '../../models';

const review: Review = {
  id: 'test',
  author: 'Test Author',
  publish_date: new Date(),
  rating: 1.5,
};

const onClick = () => null;

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ReviewListItem review={review} onClick={onClick} />, div);
});
