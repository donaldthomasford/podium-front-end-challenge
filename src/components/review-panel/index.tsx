import React, { SFC } from 'react';
import { ReviewDetails } from '../../models';
import Rating from '../rating';

import styles from './index.css';

interface Props {
  review: ReviewDetails;
}

const getHeaderItem = (label: string, value: string | JSX.Element) => {
  return (
    <div className={styles.headerItem}>
      <div className={styles.headerTitle}>
        {label}
      </div>
      <div className={styles.headerValue}>
        {value}
      </div>
    </div>
  );
};

const ReviewPanel: SFC<Props> = (props) => {
  const { review } = props;
  const { rating, publish_date, author, body } = review;

  return (
    <div className={styles.reviewPanel}>
      <div className={styles.header}>
        {getHeaderItem('Author:', author)}
        {getHeaderItem('Publish Date:', publish_date.toLocaleDateString())}
        {getHeaderItem('Rating:', <Rating rating={rating}/>)}
      </div>
      <div className={styles.body}>
        <div className={styles.bodyTitle}>
          Comments
        </div>
        <div className={styles.bodyText}>
          {body}
        </div>
      </div>
    </div>
  );
};

export default ReviewPanel;
