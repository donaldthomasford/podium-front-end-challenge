enum SortDirection {
  none,
  asc,
  desc,
}

export default SortDirection;
