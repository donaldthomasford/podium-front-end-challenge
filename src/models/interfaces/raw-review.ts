export default interface RawReview {
  rating: number;
  publish_date: string;
  id: string;
  author: string;
}