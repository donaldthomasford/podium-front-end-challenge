import Review from './review';

export default interface ReviewDetails extends Review {
  body: string;
}
