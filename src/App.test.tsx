import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import * as ReviewService from './services/reviews';
import sinon from 'sinon';

import { Review, ReviewDetails } from './models';

const testReviews: Review[] = [
  {
    id: 'test',
    author: 'Test Author',
    publish_date: new Date(),
    rating: 1.5,
  },
  {
    id: 'test 2',
    author: 'Test Author 2',
    publish_date: new Date(),
    rating: 2.5,
  }
];

const testReview: ReviewDetails = { ...testReviews[0], body: 'review body' };

const mockReviewListCall = sinon.stub(ReviewService, 'fetchReviewList');
const mockReviewCall = sinon.stub(ReviewService, 'fetchReview');

mockReviewListCall.resolves(testReviews);
mockReviewCall.resolves(testReview);

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});
