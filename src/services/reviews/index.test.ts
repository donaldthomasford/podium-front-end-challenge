import { processReview, processReviewList } from './';
import { Review, ReviewDetails, RawReview, RawReviewDetails } from '../../models';

const rawTestReviews: RawReview[] = [
  {
    id: 'test',
    author: 'Test Author',
    publish_date: '2018-08-01',
    rating: 1.5,
  },
  {
    id: 'test 2',
    author: 'Test Author 2',
    publish_date: '2018-08-02',
    rating: 2.5,
  }
];

const testReviews: Review[] = [
  {
    ...rawTestReviews[0],
    publish_date: new Date('2018-08-01'),
  },
  {
    ...rawTestReviews[1],
    publish_date: new Date('2018-08-02'),
  }
];

const rawTestReview: RawReviewDetails = { ...rawTestReviews[0], body: 'review body' };
const testReview: ReviewDetails = { ...testReviews[0], body: 'review body' };

it('should convert the date string to a Date object in each review in the list', () => {
  const result = processReviewList(rawTestReviews);
  expect(result).toEqual(testReviews);
});

it('should convert the date string to a Date object in the review detail', () => {
  const result = processReview(rawTestReview);
  expect(result).toEqual(testReview);
});
