import { Review, ReviewDetails, RawReview, RawReviewDetails } from '../../models';

const BASE_URL = 'http://shakespeare.podium.co/api/reviews';
// @TODO, this is not secure, make sure to incorporate a more secure authentication system
// that does not rely on hard coded tokens
const token = 'koOheljmQX';

const LIST_PARAMS = {
  headers: new Headers({
    'Authorization': token,
  }),
};

const detailsCache: { [id: string]: ReviewDetails } = {};

type ResponseData<T> = { data: T };

export function fetchReviewList(): Promise<Review[]> {
  // @TODO add catch statements for if this request fails of if .json() fails
  return fetch(BASE_URL, LIST_PARAMS)
    .then(res => res.json())
    .then((data: ResponseData<RawReview[]>) => processReviewList(data.data));
}

export function fetchReview(id: string): Promise<ReviewDetails> {
  // return the cached info if we have already fetched it once
  if (detailsCache[id] !== null && detailsCache[id] !== undefined) {
    return Promise.resolve(detailsCache[id]);
  }

  // @TODO add catch statements for if this request fails of if .json() fails
  return fetch(`${BASE_URL}/${id}`, LIST_PARAMS)
    .then(res => res.json())
    .then((data: ResponseData<RawReviewDetails>) => processReview(data.data));
}

export function processReviewList(rawReviews: RawReview[]): Review[] {
  return rawReviews.map(datum => ({ ...datum, publish_date: new Date(datum.publish_date) }));
}

export function processReview(rawReview: RawReviewDetails): ReviewDetails {
  const details = { ...rawReview, publish_date: new Date(rawReview.publish_date) };
  detailsCache[rawReview.id] = details;

  return details;
}
