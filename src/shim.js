// I used an older version of Create React App and already ejected,
// but found this fix for preventing a warning from happening in the tests
// https://github.com/facebook/jest/issues/4545#issuecomment-332762365

global.requestAnimationFrame = (callback) => {
  setTimeout(callback, 0);
};
