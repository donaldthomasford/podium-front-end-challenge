export const reviewPanel: string;
export const header: string;
export const headerItem: string;
export const headerTitle: string;
export const headerValue: string;
export const body: string;
export const bodyTitle: string;
export const bodyText: string;
